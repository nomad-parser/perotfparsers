#
# Copyright The NOMAD Authors.
#
# This file is part of NOMAD. See https://nomad-lab.eu for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from nomad.datamodel import EntryArchive
from nomad.parsing import MatchingParser

from nomad.datamodel.metainfo.eln.application_kit.perotf import peroTF_TFL_GammaBox_EQEmeasurement

from nomad.datamodel.metainfo.eln import SolarCellEQE
from nomad.datamodel.metainfo.eln.helper.eqe_archive import get_eqe_archive
from nomad.datamodel.metainfo.eln.helper.eqe_parser import EQEAnalyzer

import json, os, datetime

'''
This is a hello world style example for an example parser/converter.
'''

def headerlines(file):
    header = 0
    with open(file, "r") as f:
        for i, line in enumerate(f):  
            if line.strip():
                header+=1
            if line.startswith("POINTS IN SPECTRUM END"):
                break
    return header

class EQEParser(MatchingParser):
    def __init__(self):
        super().__init__(
            name='parsers/perotfeqe', code_name='PEROTFEQE', code_homepage='https://www.example.eu/',
            supported_compressions=['gz', 'bz2', 'xz']
        )

    def parse(self, mainfile: str, archive: EntryArchive, logger):
        # Log a hello world, just to get us started. TODO remove from an actual parser.
        
        if archive.data is not None:
            return 
        
        header_lines = headerlines(mainfile) 
        eqe_dict = EQEAnalyzer(mainfile, header_lines = header_lines).eqe_dict()
        eqem = peroTF_TFL_GammaBox_EQEmeasurement()
        sc_eqe = SolarCellEQE()
        sc_eqe.header_lines = header_lines
        get_eqe_archive(eqe_dict, mainfile, sc_eqe, logger)

        archive.metadata.entry_name = os.path.basename(mainfile)

        
        sample_archive_name = os.path.basename(mainfile).split('.')[0] + ".archive.json"
        eqem.samples = [f'../upload/archive/mainfile/{sample_archive_name}#data']
        
        eqem.name = f"EQE Mesaurement of {os.path.basename(mainfile).split('.')[0]}"
        sc_eqe.eqe_data_file = os.path.basename(mainfile)
        eqem.datetime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
        eqem.eqe_data = [sc_eqe]

        # archive.data = eqem

        file_name = f'{os.path.basename(mainfile)}.archive.json'
        if not archive.m_context.raw_path_exists(file_name):
            eqem_entry = eqem.m_to_dict(with_root_def=True)
            with archive.m_context.raw_file(file_name, 'w') as outfile:
                json.dump({"data": eqem_entry}, outfile)
            archive.m_context.process_updated_raw_file(file_name)
