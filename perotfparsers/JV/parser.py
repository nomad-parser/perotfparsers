#
# Copyright The NOMAD Authors.
#
# This file is part of NOMAD. See https://nomad-lab.eu for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from nomad.datamodel import EntryArchive
from nomad.parsing import MatchingParser

from nomad.datamodel.metainfo.eln.application_kit.perotf import peroTF_CR_SolSimBox_JVmeasurement # , peroTF_TFL_GammaBox_JVmeasurement

from nomad.datamodel.metainfo.eln.helper.jv_archive import get_jv_archive
from nomad.datamodel.metainfo.eln.helper.KIT_jv_parser import get_jv_data

import json, os, datetime
import inspect
'''
This is a hello world style example for an example parser/converter.
'''


class JVParser(MatchingParser):
    def __init__(self):
        super().__init__(
            name='parsers/perotfjv', code_name='PEROTFJV', code_homepage='https://www.example.eu/',
            supported_compressions=['gz', 'bz2', 'xz']
        )

    def parse(self, mainfile: str, archive: EntryArchive, logger):
        # Log a hello world, just to get us started. TODO remove from an actual parser.
        
        
        if archive.data is not None:
            return 
        jv_dict = get_jv_data(mainfile)
        jvm = peroTF_CR_SolSimBox_JVmeasurement()
        get_jv_archive(jv_dict, mainfile, jvm)

        archive.metadata.entry_name = os.path.basename(mainfile)

        
        sample_archive_name = os.path.basename(mainfile).split('.')[0] + ".archive.json"
        jvm.samples = [f'../upload/archive/mainfile/{sample_archive_name}#data']
        
        jvm.name = f"JV Measurement of {os.path.basename(mainfile).split('.')[0]}"
        jvm.data_file = os.path.basename(mainfile)
        jvm.datetime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
        
        # archive.data = jvm
        file_name = f'{os.path.basename(mainfile)}.archive.json'
        if not archive.m_context.raw_path_exists(file_name):
            jvm_entry = jvm.m_to_dict(with_root_def=True)
            with archive.m_context.raw_file(file_name, 'w') as outfile:
                json.dump({"data": jvm_entry}, outfile)
            archive.m_context.process_updated_raw_file(file_name)
